default_packages:
  pkg.installed:
    - pkgs:
      - vim
      - tcpdump
      - ntp

/etc/resolv.conf:
  file.append:
    - text: nameserver 
