set nu!
set magic
set tabstop=2
set smartindent
set autoindent
set shiftwidth=2
set wrap
set incsearch
set ignorecase
set smartcase
set wildmode=list:full
cmap w!! %sudo! tee > /dev/null %

set showmode
set showcmd
set backspace=indent,eol,start
set ruler
set hlsearch

autocmd BufRead,BufNewFile *.sls,*.yml,*.yaml set ts=2 sw=2 expandtab
