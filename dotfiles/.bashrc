## Add you prefered aliases
alias ducks='du -cksh * | sort -h'
alias states="salt-call state.show_highstate --out=json | grep __sls__ | sed 's/, *$//' | sort -u | awk '{print $2}'"
## Setting pretty colors to PS1
blue="\033[38;5;32m\]\033[48;5;256m\]"
gray="\033[38;5;255m\]\033[48;5;244m\]"
suse="\[\033[38;5;42m\]\033[48;5;22m\]"
reset="\[\033[0m\]"
debian="\[\033[38;5;15m\]\033[48;5;160m\]"
alpine="\[\033[38;5;231m\]\033[48;5;25m\]"
ubuntu="\[\033[38;5;208\]\033['48;5;090m\']"

left=$(awk -F '"' '(/^NAME/) || (/^VERSION_ID/) {printf($2 (NR==1 ? " " : ""))}' /etc/os-release)
dist=$(awk -F '=' '(/^ID=/) {printf($2)}' /etc/os-release)

# Green colors for Suse, red for debian, blue for alpine
if [ "$dist" = "opensuse" ]; then
	export PS1="$suse$left:$gray\w$\n$reset> "
elif [ "$dist" = "debian" ]; then
	export PS1="$debian$left:$gray\w$\n$reset> "
elif [ "$dist" = "alpine" ]; then
	export PS1="$alpine$left:$gray\w$\n$reset> "
elif [ "$dist" = "ubuntu" ]; then
	export PS1="$ubuntu$left:$gray\w$\n$reset> "
else
	export PS1="What black magic are you playing with?:\w$\n$reset> "
fi
